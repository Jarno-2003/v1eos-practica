#include "shell.hh"
#include <stdio.h>
#include <string>
#include <iostream>
using namespace std;

string prompt_inlezen(){
        int fd = syscall(SYS_open, "prompt.txt", O_RDONLY, 0755);
        char byte[1];
        string prom = "";
        while(syscall(SYS_read, fd, byte, 1)){
                prom += byte;
        }
        return prom;
}

int main(){
	string input;
	string prompt = prompt_inlezen();

	while(true){
		cout << prompt;
		getline(cin, input);
		if (input == "new_file") new_file();
		else if (input == "ls") list();
		else if (input == "src") src();
		else if (input == "find") find();
		else if (input == "seek") seek();
		else if (input == "exit") return 0;
		else if (input == "quit") return 0;
		else if (input == "error") return 1;
	
		if (cin.eof()) return 0;
	}
}      // EOF is een exit

void new_file(){ // ToDo: Implementeer volgens specificatie.
	cout << "Hoe wil je dat het bestand heet?:" << endl;
	string bestands_naam;
	getline(cin, bestands_naam);
	cout << "Type nu de inhoud van je bestand. Type '<EOF>' om af te sluiten." << endl;
	bool doorgaan = true;
	string bestands_info = "";
	while(doorgaan){
		string info = "";
		getline(cin, info);
		if(info == "<EOF>"){
			doorgaan = false;
		}else{
			bestands_info += info + "\n";
		}
	}
	const char *sc_bestands_naam = bestands_naam.c_str();
	const char *sc_bestands_info = bestands_info.c_str();
	int bestand_maken = syscall(SYS_creat, sc_bestands_naam, 0755);
	int info_naar_bestand = syscall(SYS_write, bestand_maken, sc_bestands_info, bestands_info.size());
	close(info_naar_bestand);
	close(bestand_maken);
	cout << "Bestand aangemaakt!\n";
}

void list(){
	int pid = syscall(SYS_fork);
	const char* args[] = {"/bin/ls", "-la", 0};
	if(pid == 0){
		syscall(SYS_execve, args[0], args, 0);
	}
	sleep(1);
}

void find(){
	cout << "Op welk woord wil je zoeken?: ";
    	string zoekwoord;
    	cin >> zoekwoord;
    	int fd[2];
   	syscall(SYS_pipe, &fd);
   	int pid = syscall(SYS_fork);
    	if (pid==0){
        	int pid2 = syscall(SYS_fork);
        	if (pid2==0){
        		syscall(SYS_close, fd[0]);
            		syscall(SYS_dup2, fd[1], 1);
            		const char* args[] = { "/usr/bin/find", ".", NULL };
            		int find = syscall(SYS_execve, args[0], args, NULL);
            	} else {
            		syscall(SYS_close, fd[1]);
            		syscall(SYS_dup2, fd[0], 0);
            		const char* args2[] = { "/bin/grep", zoekwoord.c_str(), NULL};
            		syscall(SYS_execve, args2[0], args2, NULL);
            		int exit = 0;
            		int exit_status = syscall(SYS_wait4, pid2, NULL);
            	}
	} else if (pid>0){
        	int exit = 0;
        	int exit_status = syscall(SYS_wait4, pid, NULL);
    	} 
	sleep(1);
}

void seek(){
	string seek = "seek.txt";
  	string loop = "loop.txt"; 

  	int create_seek = syscall(SYS_open, seek.c_str(), O_CREAT | O_RDWR | O_TRUNC, 0666);
 	int write_seek = syscall(SYS_write, create_seek, "x", 1);
  	int lseek = syscall(SYS_lseek, create_seek, 5000000, SEEK_CUR);
  	int write_seek_2 = syscall(SYS_write, create_seek, "x", 1);

  	if ((syscall(write_seek) == 0) && (syscall(lseek) == 0) && (syscall(write_seek_2) == 0)) {
 		cout << "error seek\n";
 	}
  
	int create_loop = syscall(SYS_open, loop.c_str(), O_CREAT | O_RDWR | O_TRUNC, 0666);
  	int write_loop_0 = syscall(SYS_write, create_loop, "x", 1);
  	for (unsigned int i = 0; i < 5000000; i++) {
    		int write_loop_1 = syscall(SYS_write, create_loop, "\0", 1);
  	}
  	int write_loop_2 = syscall(SYS_write, create_loop, "x", 1);
  	if ((syscall(write_loop_0) == 0) && (syscall(write_loop_2) == 0)) {
     		cout << "loop error\n";
  	}
}

void src(){
	int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755);
	char byte[1];
	while(syscall(SYS_read, fd, byte, 1))
		cout << byte;
	}

