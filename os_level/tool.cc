#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

void translate(string line, string argument, string & zin){
	zin += " " + line;
}

int main(int argc, char *argv[]){
	string line;
	if(argc != 2){
		cerr << "Deze functie heeft exact 1 argument nodig" << endl;
		return -1;
	}
	cout << "Welk woord wilt u toevoegen?:\n";
	string zin = argv[1];
	while(getline(cin, line)){
		translate(line, argv[1], zin);
		cout << "\n" << zin << endl;
		cout << "\nType het volgende woord:\n";
	} 
	return 0;
}
